/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ADMIN
 */
public class KhachHangDAO {
    private Connection conn;

    public KhachHangDAO() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection("jdbc:sqlserver://localhost;databaseName=DBTapHoa;"
                    + "user=sa;password=123456;encrypt=true;trustServerCertificate=true");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean ThemKhachHang(KhachHang kh) {
        String sql = "INSERT INTO KhachHang(MaKhachHang, TenKhachHang, GioiTinh, DiaChi, SoDienThoai, Diem) "
                + "VALUES(?,?,?,?,?,?)";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, kh.getMaKH());
            ps.setString(2, kh.getTenKH());
            ps.setBoolean(3, kh.isGioiTinh());
            ps.setString(4, kh.getDiaChi());
            ps.setString(5, kh.getsDT());
            ps.setInt(6, 0);
            return ps.executeUpdate() > 0;

        } catch (SQLException e) {
        }

        return false;
    }

    public boolean SuaKhachHang(KhachHang kh, String maKH) {
        String sql = "UPDATE KhachHang SET  TenKhachHang = ?, GioiTinh = ?, DiaChi = ?, SoDienThoai = ?"
                + " WHERE MaKhachHang = '" + maKH + "' ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, kh.getTenKH());
            ps.setBoolean(2, kh.isGioiTinh());
            ps.setString(3, kh.getDiaChi());
            ps.setString(4, kh.getsDT());

            return ps.executeUpdate() > 0;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }


    public boolean congDiem(int diem, String maKH) {
        String sql = "UPDATE KhachHang SET Diem = Diem + " + diem
                + " Where MaKhachHang = '" + maKH + "' ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
//            ps.setInt(1,diemThuong.(getDiemThuong()));

            return ps.executeUpdate() > 0;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean XoaKhachHang(String maKH) {
        String sql = "DELETE FROM KhachHang  WHERE MaKhachHang = '" + maKH + "' ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);

            return ps.executeUpdate() > 0;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public ArrayList<KhachHang> getListKhachHang() {
        ArrayList<KhachHang> list = new ArrayList<>();
        String sql = "SELECT * FROM KhachHang";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                KhachHang kh = new KhachHang();
                kh.setMaKH(rs.getString("MaKhachHang"));
                kh.setTenKH(rs.getString("TenKhachHang"));
                kh.setGioiTinh(rs.getBoolean("GioiTinh"));
                kh.setDiaChi(rs.getString("DiaChi"));
                kh.setsDT(rs.getString("SoDienThoai"));
                kh.setDiemThuong(rs.getInt("Diem"));
                list.add(kh);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<KhachHang> FindKH(String a[]) {
        ArrayList<KhachHang> list = new ArrayList<>();
        String sql = "SELECT * FROM KhachHang WHERE "
                + "MaKhachHang LIKE '%" + a[0] + "%' "
                + "AND TenKhachHang LIKE N'%" + a[1] + "%' "
                + "AND GioiTinh '%" + a[2] + "%' "
                + "AND DiaChi LIKE N'%" + a[3] + "%' "
                + "AND SoDienThoai LIKE '%" + a[4] + "%' "
                + "AND Diem LIKE '%" + a[5] + "%' ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                KhachHang hh = new KhachHang();
                hh.setMaKH(rs.getString("MaKhachHang"));
                hh.setTenKH(rs.getString("TenKhachHang"));
                hh.setGioiTinh(rs.getBoolean("GioiTinh"));
                hh.setDiaChi(rs.getString("DiaChi"));
                hh.setsDT(rs.getString("SoDienThoai"));
                hh.setDiemThuong(rs.getInt("Diem"));

                list.add(hh);
            }

        } catch (Exception h) {
            h.printStackTrace();
        }
        return list;
    }

    public ArrayList<KhachHang> FindKH(String a) {
        ArrayList<KhachHang> list = new ArrayList<>();
        String sql = "SELECT * FROM KhachHang WHERE "
                + "MaKhachHang Like '%" + a + "%' "
                + "OR TenKhachHang LIKE N'%" + a + "%' "
                + "OR GioiTinh LIKE N'%" + a + "%' "
                + "OR DiaChi LIKE '%" + a + "%' "
                + "OR SoDienThoai LIKE '%" + a + "%' "
                + "OR Diem LIKE '%" + a + "%' ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                KhachHang hh = new KhachHang();
                hh.setMaKH(rs.getString("MaKhachHang"));
                hh.setTenKH(rs.getString("TenKhachHang"));
                hh.setGioiTinh(rs.getBoolean("GioiTinh"));
                hh.setDiaChi(rs.getString("DiaChi"));
                hh.setsDT(rs.getString("SoDienThoai"));
                hh.setDiemThuong(rs.getInt("Diem"));

                list.add(hh);
            }

        } catch (Exception h) {
            h.printStackTrace();
        }
        return list;
    }

    public ArrayList<KhachHang> FindKH(String a, String b) {
        ArrayList<KhachHang> list = new ArrayList<>();
        String sql = "SELECT * FROM KhachHang WHERE " + a + " LIKE N'%" + b + "%' ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                KhachHang hh = new KhachHang();
                hh.setMaKH(rs.getString("MaKhachHang"));
                hh.setTenKH(rs.getString("TenKhachHang"));
                hh.setGioiTinh(rs.getBoolean("GioiTinh"));
                hh.setDiaChi(rs.getString("DiaChi"));
                hh.setsDT(rs.getString("SoDienThoai"));
                hh.setDiemThuong(rs.getInt("Diem"));

                list.add(hh);
            }

        } catch (Exception h) {
            h.printStackTrace();
        }
        return list;
    }


    public static void main(String[] args) {
        new KhachHangDAO();
    }
}
