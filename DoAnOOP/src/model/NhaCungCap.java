/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;


/**
 * @author ADMIN
 */
public class NhaCungCap {
    private String maNCC;
    private String tenNCC;
    private String SDT;
    private String diaChi;

    /**
     *
     */
    public NhaCungCap() {
    }

    public NhaCungCap(String maNCC, String tenNCC, String SDT, String diaChi) {
        this.maNCC = maNCC;
        this.tenNCC = tenNCC;
        this.SDT = SDT;
        this.diaChi = diaChi;
    }

    public String getMaNCC() {
        return maNCC;
    }

    public void setMaNCC(String maNCC) {
        this.maNCC = maNCC;
    }

    public String getTenNCC() {
        return tenNCC;
    }

    public void setTenNCC(String tenNCC) {
        this.tenNCC = tenNCC;
    }

    public String getSDT() {
        return SDT;
    }

    public void setSDT(String SDT) {
        this.SDT = SDT;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }


}
