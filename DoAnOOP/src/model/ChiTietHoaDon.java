/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 * @author ADMIN
 */
public class ChiTietHoaDon {
    private String maCT, maHD, maHH, tenHH, donViTinh;
    private int soLuong;
    private double giaTien;
    private double thanhTien;

    public ChiTietHoaDon() {
    }

    public ChiTietHoaDon(String maCT, String maHD, String maHH, String tenHH, String donViTinh, int soLuong, double giaTien, double thanhTien) {
        this.maCT = maCT;
        this.maHD = maHD;
        this.maHH = maHH;
        this.tenHH = tenHH;
        this.donViTinh = donViTinh;
        this.soLuong = soLuong;
        this.giaTien = giaTien;
        this.thanhTien = thanhTien;
    }

    public String getMaCT() {
        return maCT;
    }

    public void setMaCT(String maCT) {
        this.maCT = maCT;
    }

    public String getMaHD() {
        return maHD;
    }

    public void setMaHD(String maHD) {
        this.maHD = maHD;
    }

    public String getMaHH() {
        return maHH;
    }

    public void setMaHH(String maHH) {
        this.maHH = maHH;
    }

    public String getTenHH() {
        return tenHH;
    }

    public void setTenHH(String tenHH) {
        this.tenHH = tenHH;
    }

    public String getDonViTinh() {
        return donViTinh;
    }

    public void setDonViTinh(String donViTinh) {
        this.donViTinh = donViTinh;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }

    public double getGiaTien() {
        return giaTien;
    }

    public void setGiaTien(double giaTien) {
        this.giaTien = giaTien;
    }

    public double getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(double thanhTien) {
        this.thanhTien = thanhTien;
    }


    // hàm tăng số lượng khi thêm hàng hóa trong bảng hàng hóa vào bảng hóa đơn
    public void tangSoluong() {
        this.soLuong++;
        this.thanhTien = this.soLuong * this.giaTien;
    }

    public void tangSoluong(int sl) {
        this.soLuong += sl;
        this.thanhTien = this.soLuong * this.giaTien;
    }

    /* hàm giảm số lượng hàng hóa trong bảng hóa đơn khi muốn xóa 1 sản phẩm trong 
     bảng hóa đơn */
    public void giamSoluong() {
        this.soLuong--;
        this.thanhTien = this.soLuong * this.giaTien;
    }

}
