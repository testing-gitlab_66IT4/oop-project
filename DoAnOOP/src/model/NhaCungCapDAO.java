package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author KaNiSm
 */
public class NhaCungCapDAO {
    private Connection conn;
//    ArrayList<NhaCungCap> list = new ArrayList<>();

    public NhaCungCapDAO() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection("jdbc:sqlserver://localhost;databaseName=DBTapHoa;"
                    + "user=sa;password=123456;encrypt=true;trustServerCertificate=true");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Thêm nhà cung cấp
    public boolean ThemNCC(NhaCungCap ncc) {
        String sql = "INSERT INTO NhaCungCap(MaNhaCungCap, TenNhaCungCap, SoDienThoai, DiaChi) "
                + "VALUES(?,?,?,?)";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, ncc.getMaNCC());
            ps.setString(2, ncc.getTenNCC());
            ps.setString(3, ncc.getSDT());
            ps.setString(4, ncc.getDiaChi());
            return ps.executeUpdate() > 0;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    //sửa nhà cung cấp
    public boolean SuaNCC(NhaCungCap ncc, String maNCC) {
        String sql = "UPDATE NhaCungCap SET TenNhaCungCap = ?, SoDienThoai = ?, DiaChi = ?"
                + " WHERE MaNhaCungCap = '" + maNCC + "' ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, ncc.getTenNCC());
            ps.setString(2, ncc.getSDT());
            ps.setString(3, ncc.getDiaChi());

            return ps.executeUpdate() > 0;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public boolean XoaNCC(String maNCC) {
        String sql = "DELETE FROM NhaCungCap WHERE MaNhaCungCap = '" + maNCC + "'";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);

            return ps.executeUpdate() > 0;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }


    public ArrayList<NhaCungCap> getListNhaCungCap() {
        ArrayList<NhaCungCap> list = new ArrayList<>();
        String sql = "SELECT * FROM NhaCungCap";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                NhaCungCap ncc = new NhaCungCap();
                ncc.setMaNCC(rs.getString("MaNhaCungCap"));
                ncc.setTenNCC(rs.getString("TenNhaCungCap"));
                ncc.setSDT(rs.getString("SoDienThoai"));
                ncc.setDiaChi(rs.getString("DiaChi"));
                list.add(ncc);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public NhaCungCap getNhaCungCap(String maNCC) {
        NhaCungCap ncc = new NhaCungCap();
        String sql = "SELECT * FROM NhaCungCap WHERE MaNhaCungCap = " + maNCC;
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            rs.next();
            ncc.setTenNCC(rs.getString("MaNhaCungCap"));
            ncc.setTenNCC(rs.getString("TenNhaCungCap"));
            ncc.setSDT(rs.getString("SoDienThoai"));
            ncc.setDiaChi(rs.getString("DiaChi"));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ncc;
    }

    public ArrayList<String> getTenNhaCungCap() {
        ArrayList<String> tenNCC = new ArrayList<>();
        String sql = "SELECT ID FROM NhaCungCap";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                tenNCC.add(rs.getString("MaNhaCungCap"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tenNCC;
    }

    public ArrayList<NhaCungCap> FindNCC(String a[]) {
        ArrayList<NhaCungCap> list = new ArrayList<>();
        String sql = "SELECT * FROM NhaCungCap WHERE "
                + "MaNhaCungCap LIKE '%" + a[0] + "%' "
                + "AND TenNhaCungCap LIKE N'%" + a[1] + "%' "
                + "AND SoDienThoai '%" + a[2] + "%' "
                + "AND DiaChi LIKE N'%" + a[3] + "%' ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                NhaCungCap hh = new NhaCungCap();
                hh.setMaNCC(rs.getString("MaNhaCungCap"));
                hh.setTenNCC(rs.getString("TenNhaCungCap"));
                hh.setSDT(rs.getString("SoDienThoai"));
                hh.setDiaChi(rs.getString("DiaChi"));

                list.add(hh);
            }

        } catch (Exception h) {
            h.printStackTrace();
        }
        return list;
    }

    public ArrayList<NhaCungCap> FindNCC(String a) {
        ArrayList<NhaCungCap> list = new ArrayList<>();
        String sql = "SELECT * FROM NhaCungCap WHERE "
                + "MaNhaCungCap Like '%" + a + "%' "
                + "OR TenNhaCungCap LIKE N'%" + a + "%' "
                + "OR SoDienThoai LIKE '%" + a + "%' "
                + "OR DiaChi LIKE N'%" + a + "%' ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                NhaCungCap hh = new NhaCungCap();
                hh.setMaNCC(rs.getString("MaNhaCungCap"));
                hh.setTenNCC(rs.getString("TenNhaCungCap"));
                hh.setSDT(rs.getString("SoDienThoai"));
                hh.setDiaChi(rs.getString("DiaChi"));

                list.add(hh);
            }

        } catch (Exception h) {
            h.printStackTrace();
        }
        return list;
    }

    public ArrayList<NhaCungCap> FindNCC(String a, String b) {
        ArrayList<NhaCungCap> list = new ArrayList<>();
        String sql = "SELECT * FROM NhaCungCap WHERE " + a + " LIKE N'%" + b + "%' ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                NhaCungCap hh = new NhaCungCap();
                hh.setMaNCC(rs.getString("MaNhaCungCap"));
                hh.setTenNCC(rs.getString("TenNhaCungCap"));
                hh.setSDT(rs.getString("SoDienThoai"));
                hh.setDiaChi(rs.getString("DiaChi"));

                list.add(hh);
            }

        } catch (Exception h) {
            h.printStackTrace();
        }
        return list;
    }


    public static void main(String[] args) {
        new NhaCungCapDAO();
//        NhaCungCap ncc = new NhaCungCap();
//        ncc.setTenNCC("name");
//        ncc.setSDT("0221");
//        ncc.setDiaChi("DC");
//        new NhaCungCapDAO().ThemNCC(ncc);
    }

}
