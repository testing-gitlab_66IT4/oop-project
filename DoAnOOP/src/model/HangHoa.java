/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.ArrayList;

/**
 * @author ADMIN
 */
public class HangHoa {
    private String maHH, tenHH, loai, donViTinh, viTri;
    private int soLuong;
    private double giaTien;

    public HangHoa() {
    }

    public HangHoa(String maHH, String tenHH, String loai, String donViTinh, String viTri, int soLuong, double giaTien) {
        this.maHH = maHH;
        this.tenHH = tenHH;
        this.loai = loai;
        this.donViTinh = donViTinh;
        this.viTri = viTri;
        this.soLuong = soLuong;
        this.giaTien = giaTien;
    }

    public String getDonViTinh() {
        return donViTinh;
    }

    public void setDonViTinh(String donViTinh) {
        this.donViTinh = donViTinh;
    }

    public String getMaHH() {
        return maHH;
    }

    public void setMaHH(String maHH) {
        this.maHH = maHH;
    }

    public String getTenHH() {
        return tenHH;
    }

    public void setTenHH(String tenHH) {
        this.tenHH = tenHH;
    }

    public String getLoai() {
        return loai;
    }

    public void setLoai(String loai) {
        this.loai = loai;
    }

    public String getViTri() {
        return viTri;
    }

    public void setViTri(String viTri) {
        this.viTri = viTri;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }

    public double getGiaTien() {
        return giaTien;
    }

    public void setGiaTien(double giaTien) {
        this.giaTien = giaTien;
    }

    public void giamSoluong() {
        this.soLuong--;
    }

    public void giamSoluong(int sl) {
        this.soLuong -= sl;
    }

    public void TangSoluong() {
        this.soLuong++;
    }

}
