
package model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ADMIN
 */
public class UserDAO {
    List<User> ls = new ArrayList<>();

    public UserDAO() {
        ls.add(new User("admin", "123456", true));
    }

    public boolean checkLogin(String tendangnhap, String matkhau) {
        for (User u : ls) {
            if (u.getTenDangNhap().equals(tendangnhap)
                    && u.getMatKhau().equals(matkhau)) {
                return true;
            }
        }
        return false;
    }
}