
package model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ADMIN
 */
public class User {
    private String tenDangNhap;
    private String matKhau;
    private boolean role;

    public User() {
    }

    public User(String tenDangNhap, String matKhau, boolean role) {
        this.tenDangNhap = tenDangNhap;
        this.matKhau = matKhau;
        this.role = role;
    }

    public String getTenDangNhap() {
        return tenDangNhap;
    }

    public void setTenDangNhap(String tenDangNhap) {
        this.tenDangNhap = tenDangNhap;
    }

    public String getMatKhau() {
        return matKhau;
    }

    public void setMatKhau(String matKhau) {
        this.matKhau = matKhau;
    }

    public boolean isRole() {
        return role;
    }

    public void setRole(boolean role) {
        this.role = role;
    }

}
