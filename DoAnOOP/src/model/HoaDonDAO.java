/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import model.HoaDon;
import model.ChiTietHoaDon;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ADMIN
 */
public class HoaDonDAO {
    private Connection conn;

    public HoaDonDAO() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection("jdbc:sqlserver://localhost;databaseName=DBTapHoa;"
                    + "user=sa;password=123456;encrypt=true;trustServerCertificate=true");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean ThemHoaDonCoKH(HoaDon hd) {
        String sql = "INSERT INTO HoaDon(MaHoaDon, NgayTao, TenKhachHang, TongTien) "
                + "VALUES(?,?,?,?)";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, hd.getMaHD());
            ps.setDate(2, new Date(hd.getNgayTao().getTime()));
            ps.setString(3, hd.getTenKH());
            ps.setDouble(4, hd.getTongTien());
            return ps.executeUpdate() > 0;

        } catch (SQLException e) {
        }

        return false;
    }

    public boolean ThemHoaDonKoKH(HoaDon hd) {
        String sql = "INSERT INTO HoaDon(MaHoaDon, NgayTao, TongTien) "
                + "VALUES(?,?,?)";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, hd.getMaHD());
            ps.setDate(2, new Date(hd.getNgayTao().getTime()));
            ps.setDouble(3, hd.getTongTien());
            return ps.executeUpdate() > 0;

        } catch (SQLException e) {
        }

        return false;
    }

    public boolean ThemChiTietHoaDon(ChiTietHoaDon cthd) {
        String sql = "INSERT INTO ChiTietHoaDon(MaHoaDon, MaHangHoa, TenHangHoa, SoLuong, GiaTien, ThanhTien) "
                + "VALUES(?,?,?,?,?,?)";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, cthd.getMaHD());
            ps.setString(2, cthd.getMaHH());
            ps.setString(3, cthd.getTenHH());
            ps.setInt(4, cthd.getSoLuong());
            ps.setDouble(5, cthd.getGiaTien());
            ps.setDouble(6, cthd.getThanhTien());
            return ps.executeUpdate() > 0;

        } catch (SQLException e) {
        }

        return false;
    }

    public boolean XoaHoaDon(String maHoadon) {
        String sql = "DELETE FROM HoaDon  WHERE MaHoaDon = '" + maHoadon + "' ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);

            return ps.executeUpdate() > 0;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public ArrayList<HoaDon> getListHoaDon() {
        ArrayList<HoaDon> list = new ArrayList<>();
        String sql = "Select * FROM HoaDon";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                HoaDon hd = new HoaDon();
                hd.setMaHD(rs.getString("MaHoaDon"));
                hd.setNgayTao(rs.getDate("NgayTao"));
                hd.setTenKH(rs.getString("TenKhachHang"));
                hd.setTongTien(rs.getDouble("TongTien"));
                list.add(hd);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public HoaDon getHoaDon(String maHoaDon) {
        HoaDon hd = new HoaDon();
        String sql = "SELECT * FROM HoaDon WHERE MaHoaDon = " + maHoaDon;
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            rs.next();
            hd.setMaHD(rs.getString("MaHoaDon"));
            hd.setNgayTao(rs.getDate("NgayTao"));
            hd.setTenKH(rs.getString("TenKhachHang"));
            hd.setTongTien(rs.getDouble("TongTien"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hd;
    }

    public ArrayList<HoaDon> FindHD(String a[]) {
        ArrayList<HoaDon> list = new ArrayList<>();
        String sql = "SELECT * FROM HoaDon WHERE "
                + "MaHoaDon LIKE '%" + a[0] + "%' "
                + "AND NgayTao LIKE '%" + a[1] + "%' "
                + "AND TenKhachHang LIKE '%" + a[2] + "%' ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                HoaDon hd = new HoaDon();
                hd.setMaHD(rs.getString("MaHoaDon"));
                hd.setNgayTao(rs.getDate("NgayTao"));
                hd.setTenKH(rs.getString("TenKhachHang"));
                hd.setTongTien(rs.getDouble("TongTien"));
                list.add(hd);
            }

        } catch (Exception h) {
            h.printStackTrace();
        }
        return list;
    }

    public ArrayList<HoaDon> FindHD(String a) {
        ArrayList<HoaDon> list = new ArrayList<>();
        String sql = "SELECT * FROM HoaDon WHERE "
                + "ID MaHoaDon '%" + a + "%' "
                + "OR NgayTao LIKE '%" + a + "%' "
                + "AND TenKhachHang LIKE '%" + a + "%' ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                HoaDon hd = new HoaDon();
                hd.setMaHD(rs.getString("MaHoaDon"));
                hd.setNgayTao(rs.getDate("NgayTao"));
                hd.setTenKH(rs.getString("TenKhachHang"));
                hd.setTongTien(rs.getDouble("TongTien"));
                list.add(hd);
            }

        } catch (Exception h) {
            h.printStackTrace();
        }
        return list;
    }

    public ArrayList<HoaDon> FindHD(String a, String b) {
        ArrayList<HoaDon> list = new ArrayList<>();
        String sql = "SELECT * FROM HoaDon WHERE " + a + " LIKE '%" + b + "%' ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                HoaDon hd = new HoaDon();
                hd.setMaHD(rs.getString("MaHoaDon"));
                hd.setNgayTao(rs.getDate("NgayTao"));
                hd.setTenKH(rs.getString("TenKhachHang"));
                hd.setTongTien(rs.getDouble("TongTien"));
                list.add(hd);
            }

        } catch (Exception h) {
            h.printStackTrace();
        }
        return list;
    }

    public boolean CongDiem(KhachHang kh) {
        String sql = "INSERT INTO KhachHang(Diem) "
                + "VALUES(?)";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(6, kh.getDiemThuong());
            return ps.executeUpdate() > 0;

        } catch (SQLException e) {
        }

        return false;
    }

    public ArrayList<HoaDon> TKHD() {
        ArrayList<HoaDon> Listhd = new ArrayList<>();

        String sql1 = "SELECT * FROM HoaDon "
                + "ORDER BY NgayTao";
        try {
            PreparedStatement ps = conn.prepareStatement(sql1);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                HoaDon hd = new HoaDon();
                hd.setMaHD(rs.getString("MaHoaDon"));
                hd.setNgayTao(rs.getDate("NgayTao"));
                hd.setTenKH(rs.getString("TenKhachHang"));
                hd.setTongTien(rs.getDouble("TongTien"));
                Listhd.add(hd);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return Listhd;
    }

    public ArrayList<HoaDon> TKHD(String Tu, String Den) {
        ArrayList<HoaDon> Listhd = new ArrayList<>();

        String sql1 = "SELECT * FROM HoaDon "
                + "WHERE NgayTao >= '" + Tu + "' AND NgayTao <= '" + Den + "' "
                + "ORDER BY NgayTao";
        try {
            PreparedStatement ps = conn.prepareStatement(sql1);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                HoaDon hd = new HoaDon();
                hd.setMaHD(rs.getString("MaHoaDon"));
                hd.setNgayTao(rs.getDate("NgayTao"));
                hd.setTenKH(rs.getString("TenKhachHang"));
                hd.setTongTien(rs.getDouble("TongTien"));
                Listhd.add(hd);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return Listhd;
    }

    public static void main(String[] args) {
        new HoaDonDAO();
    }
}
