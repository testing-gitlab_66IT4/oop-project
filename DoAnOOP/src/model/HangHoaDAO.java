/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ADMIN
 */
public class HangHoaDAO {

    private Connection conn;

    public HangHoaDAO() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection("jdbc:sqlserver://localhost;databaseName=DBTapHoa;"
                    + "user=sa;password=123456;encrypt=true;trustServerCertificate=true");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean ThemHangHoa(HangHoa hh) {
        String sql = "INSERT INTO HangHoa(MaHangHoa, TenHangHoa, Loai, DonViTinh, ViTri, SoLuong, GiaTien) "
                + "VALUES(?,?,?,?,?,?,?)";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, hh.getMaHH());
            ps.setString(2, hh.getTenHH());
            ps.setString(3, hh.getLoai());
            ps.setString(4, hh.getDonViTinh());
            ps.setString(5, hh.getViTri());
            ps.setInt(6, hh.getSoLuong());
            ps.setDouble(7, hh.getGiaTien());
            return ps.executeUpdate() > 0;

        } catch (SQLException e) {
        }

        return false;
    }

    public boolean SuaHangHoa(HangHoa hh, String maHH) {
        String sql = "UPDATE HangHoa SET  TenHangHoa = ?, Loai = ?, DonViTinh = ?, ViTri = ?, SoLuong = ?, GiaTien = ? "
                + "Where MaHangHoa = '" + maHH + "' ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, hh.getTenHH());
            ps.setString(2, hh.getLoai());
            ps.setString(3, hh.getDonViTinh());
            ps.setString(4, hh.getViTri());
            ps.setInt(5, hh.getSoLuong());
            ps.setDouble(6, hh.getGiaTien());

            return ps.executeUpdate() > 0;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public boolean giamSLCuaHangHoa(int sl, String maHH) {
        String sql = "UPDATE HangHoa SET SoLuong = SoLuong - " + sl
                + " Where MaHangHoa = '" + maHH + "' ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);

            return ps.executeUpdate() > 0;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean XoaHangHoa(String maHH) {
        String sql = "DELETE FROM HangHoa  WHERE MaHangHoa = '" + maHH + "' ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);

            return ps.executeUpdate() > 0;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public ArrayList<HangHoa> getListHangHoa() {
        ArrayList<HangHoa> list = new ArrayList<>();
        String sql = "SELECT * FROM HangHoa";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                HangHoa hh = new HangHoa();
                hh.setMaHH(rs.getString("MaHangHoa"));
                hh.setTenHH(rs.getString("TenHangHoa"));
                hh.setLoai(rs.getString("Loai"));
                hh.setDonViTinh(rs.getString("DonViTinh"));
                hh.setViTri(rs.getString("ViTri"));
                hh.setSoLuong(rs.getInt("SoLuong"));
                hh.setGiaTien(rs.getDouble("GiaTien"));
                list.add(hh);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public HangHoa getHangHoa(String maHH) {
        HangHoa hh = new HangHoa();
        String sql = "SELECT * FROM HangHoa WHERE MaHangHoa = '" + maHH + "' ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            rs.next();
            hh.setMaHH(rs.getString("MaHangHoa"));
            hh.setTenHH(rs.getString("TenHangHoa"));
            hh.setLoai(rs.getString("Loai"));
            hh.setDonViTinh(rs.getString("DonViTinh"));
            hh.setViTri(rs.getString("ViTri"));
            hh.setSoLuong(rs.getInt("SoLuong"));
            hh.setGiaTien(rs.getDouble("GiaTien"));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return hh;
    }

    public ArrayList<HangHoa> FindHH(String a[]) {
        ArrayList<HangHoa> list = new ArrayList<>();
        String sql = "SELECT * FROM HangHoa WHERE "
                + "MaHangHoa LIKE '%" + a[0] + "%' "
                + "AND TenHangHoa LIKE N'%" + a[1] + "%' "
                + "AND Loai LIKE N'%" + a[2] + "%' "
                + "AND ViTri LIKE '%" + a[3] + "%' "
                + "AND SoLuong LIKE '%" + a[4] + "%' "
                + "AND GiaTien LIKE '%" + a[5] + "%' ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                HangHoa hh = new HangHoa();
                hh.setMaHH(rs.getString("MaHangHoa"));
                hh.setTenHH(rs.getString("TenHangHoa"));
                hh.setLoai(rs.getString("Loai"));
                hh.setDonViTinh(rs.getString("DonViTinh"));
                hh.setViTri(rs.getString("ViTri"));
                hh.setSoLuong(rs.getInt("SoLuong"));
                hh.setGiaTien(rs.getDouble("GiaTien"));

                list.add(hh);
            }

        } catch (Exception h) {
            h.printStackTrace();
        }
        return list;
    }

    public ArrayList<HangHoa> FindHH(String a) {
        ArrayList<HangHoa> list = new ArrayList<>();
        String sql = "SELECT * FROM HangHoa WHERE "
                + "MaHangHoa LIKE '%" + a + "%' "
                + "OR TenHangHoa LIKE N'%" + a + "%' "
                + "OR Loai LIKE N'%" + a + "%' "
                + "OR ViTri LIKE '%" + a + "%' "
                + "OR SoLuong LIKE '%" + a + "%' "
                + "OR GiaTien LIKE '%" + a + "%' ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                HangHoa hh = new HangHoa();
                hh.setMaHH(rs.getString("MaHangHoa"));
                hh.setTenHH(rs.getString("TenHangHoa"));
                hh.setLoai(rs.getString("Loai"));
                hh.setDonViTinh(rs.getString("DonViTinh"));
                hh.setViTri(rs.getString("ViTri"));
                hh.setSoLuong(rs.getInt("SoLuong"));
                hh.setGiaTien(rs.getDouble("GiaTien"));

                list.add(hh);
            }

        } catch (Exception h) {
            h.printStackTrace();
        }
        return list;
    }

    public ArrayList<HangHoa> FindHH(String a, String b) {
        ArrayList<HangHoa> list = new ArrayList<>();
        String sql = "SELECT * FROM HangHoa WHERE " + a + " LIKE N'%" + b + "%' ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                HangHoa hh = new HangHoa();
                hh.setMaHH(rs.getString("MaHangHoa"));
                hh.setTenHH(rs.getString("TenHangHoa"));
                hh.setLoai(rs.getString("Loai"));
                hh.setDonViTinh(rs.getString("DonViTinh"));
                hh.setViTri(rs.getString("ViTri"));
                hh.setSoLuong(rs.getInt("SoLuong"));
                hh.setGiaTien(rs.getDouble("GiaTien"));

                list.add(hh);
            }

        } catch (Exception h) {
            h.printStackTrace();
        }
        return list;
    }


    public static void main(String[] args) {
        new HangHoaDAO();
    }
}
