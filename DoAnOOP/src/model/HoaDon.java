/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Date;

/**
 * @author ADMIN
 */
public class HoaDon {
    private String maHD;
    private Date ngayTao;
    private String maKH;
    private String tenKH;
    private double tongTien;

    public HoaDon() {
    }

    public HoaDon(String maHD, Date ngayTao, String maKH, String tenKH, double tongTien) {
        this.maHD = maHD;
        this.ngayTao = ngayTao;
        this.maKH = maKH;
        this.tenKH = tenKH;
        this.tongTien = tongTien;
    }

    public HoaDon(String maHD, String maKH, String tenKH, double tongTien) {
        this.maHD = maHD;
        this.maKH = maKH;
        this.tenKH = tenKH;
        this.tongTien = tongTien;
    }

    public HoaDon(String maHD, Date ngayTao, double tongTien) {
        this.maHD = maHD;
        this.ngayTao = ngayTao;
        this.tongTien = tongTien;
    }


    public String getMaHD() {
        return maHD;
    }

    public void setMaHD(String maHD) {
        this.maHD = maHD;
    }

    public Date getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getMaKH() {
        return maKH;
    }

    public void setMaKH(String maKH) {
        this.maKH = maKH;
    }

    public String getTenKH() {
        return tenKH;
    }

    public void setTenKH(String tenKH) {
        this.tenKH = tenKH;
    }

    public double getTongTien() {
        return tongTien;
    }

    public void setTongTien(double tongTien) {
        this.tongTien = tongTien;
    }


}
